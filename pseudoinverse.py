import numpy as np
import scipy
from scipy import signal
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation

from numpy import linalg as LA

print ("\n")
print ("[a00 a01 a02 a03]")
print ("[a10 a11 a12 a13]")
print ("[a20 a21 a22 a23]")
print ("[a30 a31 a32 a33]")
print ("\n")
print ("Please Enter the Values from the A matrix above.")

A = np.matrix([	[1,  2,  3,  4],
				[0,  1,  0,  0],
				[0,  0,  1,  0],
				[0,  0,  0,  1] ])

for i in range(0,4):
	for j in range(0,4):
		A[i,j] = float(input("a{0}{1}: ".format(i,j)))
	print ("\n")

print ("A:\n{0}\n".format(A))

Atrans 	= A.T
AtransA = Atrans * A
AtransAinv = LA.inv(AtransA)
Apseudo = AtransAinv * Atrans

print ("Transpose of A:\n{0}\n".format(Atrans))
print ("Transpose of A Premultiplied by A:\n{0}\n".format(AtransA))
print ("Transpose of A Premultiplied by A and Inverted:\n{0}\n".format(AtransAinv))

print ("Moore-Penrose Pseudoinverse of A:\n{0}\n".format(Apseudo))
print ("Moore-Penrose Pseudoinverse Premultiplied by A (Should be I):\n{0}\n".format(Apseudo * A))
